const $ = require("jquery")
require("./style.css");

(async () => {
	$(".browser-bar")
		.html(`
			<button class="back">
				${require("../icons/2B05.svg")}
			</button>
			<button class="forward">
				${require("../icons/27A1.svg")}
			</button>
			<button class="reload">
				${require("../icons/1F504.svg")}
			</button>
			<input class="address-bar" type="text" value="https://news.ycombinator.com">
		`)
		.each(function () {
			const dispatchEvent = async event => {
				if(this.onEvent)
					await this.onEvent(event)
			}

			const addressBar = $(this)
				.find(".back")
					.click(async () => await dispatchEvent({ op: "navigateBack" }))
				.end()
				.find(".forward")
					.click(async () => await dispatchEvent({ op: "navigateForward" }))
				.end()
				.find(".reload")
					.click(async evt => await dispatchEvent({ op: evt.ctrlKey && evt.altKey ? "reset" : "reload" }))
				.end()
				.find(".address-bar")
					.keydown(async evt => {
						if(evt.key == "Enter") {
							dispatchEvent({ op: "goto", args: { url: evt.currentTarget.value } })
						}
					})

			this.setLocation = newUrl => addressBar.val(newUrl)
		})
})()
