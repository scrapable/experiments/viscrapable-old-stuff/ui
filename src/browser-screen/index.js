const $ = require("jquery")
require("./style.css")

const registerEventListeners = async (screenImg, dispatchEvent) => {
    let x, y
	let lastX = 0, lastY = 0

	const dispatchMouseMoveEvent = async () => {
		await dispatchEvent({
			op: "mouseMove",
			args: { x, y }
		})
		lastX = x
		lastY = y
	}

	screenImg.addEventListener("mousemove", async evt => {
		x = Math.floor((evt.offsetX / screenImg.clientWidth) * 800)
		y = Math.floor((evt.offsetY / screenImg.clientHeight) * 600)
	})

	setInterval(async () => {
		if(x != null && y != null && (x !== lastX || y !== lastY))
			await dispatchMouseMoveEvent()
	}, 250)

	const addMouseButtonDispatcher = evtName => {
		screenImg.addEventListener(evtName.toLowerCase(), async evt => {
			await dispatchEvent({
				op: evtName,
				args: {
					button: ["left", "middle", "right"][evt.button]
				}
			})
		})
	}

	addMouseButtonDispatcher("mouseDown")
	addMouseButtonDispatcher("mouseUp")

	screenImg.addEventListener("keydown", async evt => {
		await dispatchEvent({
			op: "keyDown",
			args: {
				code: evt.key
			}
		})
		if(evt.key === "Tab") {
			screenImg.focus()
		}
		evt.preventDefault()
	})
	screenImg.addEventListener("keyup", async evt => {
		await dispatchEvent({
			op: "keyUp",
			args: {
				code: evt.key
			}
		})
		evt.preventDefault()
	})

	screenImg.addEventListener("wheel", async evt => {
		await dispatchEvent({
			op: "scroll",
			args: {
				deltaX: evt.deltaX,
				deltaY: evt.deltaY
			}
		})
	})
}

(async () => {
    $(".browser-screen").each(async function () {
        $(this)
            .html("<img class=\"browser-screen-img noselect\" />")
            .find(".browser-screen-img")
                .attr("tabindex", $(this).attr("tabindex"))
            .end()
            .removeAttr("tabindex")

        const screenImg = $(this).find(".browser-screen-img")[0]
        
        const dispatchEvent = async event => {
            if(this.onEvent)
                await this.onEvent(event)
        }
    
        await registerEventListeners(screenImg, dispatchEvent)

        this.pushScreenshot = base64Str => {
            screenImg.setAttribute("src", `data:image/png;base64,${base64Str}`)
        }
    })
})()
