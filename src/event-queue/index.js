const $ = require("jquery")
require("./style.css")

const renderArg = (name, value) =>
    $("<li>")
        .addClass("arg")
        .text(`${name}: `)
        .append($("<span>")
            .addClass(typeof(value))
            .text(value.toString().replace(/^(\s+)$/, "\"$1\""))
        )

const renderArgs = args => args ?
    Object.keys(args).map(name => renderArg(name, args[name])) :
    []

$(".event-queue").each(function () {
    const body = $(this).find("tbody")

    this.pushEvent = event => {
        const itemJq = $("<tr>")
            .addClass("event")
        const item = itemJq[0]

        const render = event => itemJq
            .append($("<td>").addClass("op").text(event.op))
            .append($("<td>")
                .addClass("args")
                .append($("<ul>").append(renderArgs(event.args)))
            )
            .append($("<td>").addClass("delay").text(`${event.delayBefore}ms`))
            .appendTo(body)

        render(event)
        item.scrollIntoView()

        item.render = event => {
            item.innerHTML = ""
            render(event)
        }
    }

    this.removeEvent = index =>
        findEventByIndex(index).remove()

    this.updateEvent = (index, newValue) =>
        findEventByIndex(index)[0].render(newValue)

    const findEventByIndex = index =>
        body.find(".event").eq(index)

    this.swapQueue = newQueue => {
        body[0].innerHTML = ""
        newQueue.forEach(this.pushEvent)
    }
})
