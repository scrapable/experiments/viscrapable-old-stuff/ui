const $ = require("jquery")
require("./style.css");

(async () => {
	$(".recording-state-buttons")
		.html(`
			<button class="record">
				${require("../icons/record.svg")}
			</button>` +
			`<button class="play">
				${require("../icons/play.svg")}
			</button>
		`)
		.each(async function () {
			const recordButton = $(this).find("button.record")
			const playButton = $(this).find("button.play")


			const setActive = (button, active) =>
				button[active ? "addClass" : "removeClass"]("toggle-active")

			const setActiveAll = activeArr => {
				[recordButton, playButton].forEach((b, i) => setActive(b, activeArr[i]))
			}

			this.setState = newState =>
				setActiveAll({
					recording: [true, false],
					playback: [false, true],
					manual: [false, false]
				}[newState])


			recordButton.click(async () => {
				if(this.onStateChanged)
					await this.onStateChanged(this.getState() === "recording" ? "manual" : "recording")
			})

			playButton.click(async () => {
				if(this.onStateChanged)
					await this.onStateChanged(this.getState() === "playback" ? "manual" : "playback")
			})
		})
})()
