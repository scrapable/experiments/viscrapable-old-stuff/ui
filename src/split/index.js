const $ = require("jquery")
require("./style.css")

$(".split > .resizable")
    .css("position", "relative")
    .prepend($("<div>").addClass("resize-dragger"))
    .each(function () {
        const size = $(this).attr("start-size")
        const isHorizontal = $(this).closest(".split").hasClass("horizontal")
        $(this).css({
            [isHorizontal ? "width" : "height"]: size,
            [isHorizontal ? "min-width" : "min-height"]: size
        })
    })


let listener = null

const stopListening = () => {
    listener = null
    $(document.body).removeClass("noselect")
}

$(document.body)
    .mousemove(evt => {
        if(listener) listener(evt)
    })
    .mouseup(stopListening)

$(".resize-dragger")
    .mousedown(function (evt) {
        const isHorizontal = $(this).closest(".split").hasClass("horizontal")
        const startPos = evt[isHorizontal ? "pageX" : "pageY"]
        const startSize = this.parentElement[isHorizontal ? "clientWidth" : "clientHeight"]

        $(document.body).addClass("noselect")

        listener = moveEvt => {
            if(moveEvt.buttons !== 1) {
                stopListening()
                return
            }

            const pos = moveEvt[isHorizontal ? "pageX" : "pageY"]

            let newSize = startSize + (pos - startPos) * (isHorizontal ? 1 : -1)
            newSize = Math.max(newSize, 20)
            this.parentElement.style[isHorizontal ? "width" : "height"] = newSize
            this.parentElement.style[isHorizontal ? "minWidth" : "minHeight"] = newSize
        }
    })
